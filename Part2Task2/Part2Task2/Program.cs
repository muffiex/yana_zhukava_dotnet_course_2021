﻿using System;

namespace Part2Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 3 числа:");

            int firstNumber = 0;
            while (!int.TryParse(Console.ReadLine(), out firstNumber))
            {
                Console.WriteLine("Дед, ты что, цифры на клаве потерял..?");
            }

            int secondNumber = 0;
            while (!int.TryParse(Console.ReadLine(), out secondNumber))
            {
                Console.WriteLine("Интересный факт: цифорки находятся сверху и справа))");
            }

            int thirdNumber = 0;
            while (!int.TryParse(Console.ReadLine(), out thirdNumber))
            {
                Console.WriteLine("Это уже не смешно... Повторюсь: СВЕРХУ И СПРАВА!!!!!! ");
            }

            int maxValue = Math.Max(firstNumber, secondNumber);
            maxValue = Math.Max(maxValue, thirdNumber);

            int minValue = Math.Min(firstNumber, secondNumber);
            minValue = Math.Min(minValue, thirdNumber);

            if (maxValue != firstNumber && minValue != firstNumber)
            {
                Console.WriteLine($"Числа в порядке возрастания: {minValue}, {firstNumber}, {maxValue}");
                Console.WriteLine($"Числа в порядке убывания: {maxValue}, {firstNumber}, {minValue}");
                Console.WriteLine($"Среднее число: {firstNumber}");
            }
            else if (maxValue != secondNumber && minValue != secondNumber)
            {
                Console.WriteLine($"Числа в порядке возрастания: {minValue}, {secondNumber}, {maxValue}");
                Console.WriteLine($"Числа в порядке убывания: {maxValue}, {secondNumber}, {minValue}");
                Console.WriteLine($"Среднее число: {secondNumber}");
            }
            else if (maxValue != thirdNumber && minValue != thirdNumber)
            {
                Console.WriteLine($"Числа в порядке возрастания: {minValue}, {thirdNumber}, {maxValue}");
                Console.WriteLine($"Числа в порядке убывания: {maxValue}, {thirdNumber}, {minValue}");
                Console.WriteLine($"Среднее число: {thirdNumber}");
            }

            Console.WriteLine($"Максимальное значение: {maxValue}\nМинимальное значение: {minValue}");
        }
    }
}
