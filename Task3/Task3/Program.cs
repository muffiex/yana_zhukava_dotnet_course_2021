﻿using System;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0;
            Console.Write("Введите, что хотите(желательно число): ");
            int.TryParse(Console.ReadLine(), out number);
            Console.WriteLine("Ваше число: " + number);
        }
    }
}
