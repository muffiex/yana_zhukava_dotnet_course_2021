﻿using System;

namespace Part2Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите 1е число: ");

            int firstNumber = 0;
            while (!int.TryParse(Console.ReadLine(), out firstNumber))
            {
                Console.WriteLine("Повторите ввод...");
            }

            Console.Write("Введите 2е число: ");

            int secondNumber = 0;
            while (!int.TryParse(Console.ReadLine(), out secondNumber))
            {
                Console.WriteLine("Повторите ввод...");
            }

            if (firstNumber / secondNumber != 0)
            {
                if (firstNumber % secondNumber == 0)
                {
                    Console.Write($"{firstNumber} делится на {secondNumber} с целой частью {firstNumber / secondNumber} и без остатка.");
                }
                else
                {
                    Console.Write($"{firstNumber} делится на {secondNumber} c целой частью {firstNumber / secondNumber} и с остатком {firstNumber % secondNumber}.");
                }
            }
            else
            {
                Console.WriteLine($"{firstNumber} не делится на {secondNumber}.");
            }
        }
    }
}
