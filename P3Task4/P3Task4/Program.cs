﻿using System;

namespace P3Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива: ");

            int arrayLength = 0;
            while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength < 0)
            {
                Console.WriteLine("Повторите ввод..");
            }

            int[] numbers = new int[arrayLength];

            Console.Write($"Введите {arrayLength - 1} элементов массива:\n");

            int value = 0;
            for (int i = 0; i < arrayLength - 1; i++)
            {
                while (!int.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("Повторите ввод..");
                }

                numbers[i] = value;
            }

            Console.Write("Введите еще один элемент: ");

            int addedElement = 0;
            while (!int.TryParse(Console.ReadLine(), out addedElement))
            {
                Console.WriteLine("Повторите ввод..");
            }

            Console.Write($"Введите желаемую позицию (0 - {arrayLength - 1}): ");

            int addedElementPosition = 0;
            while (!int.TryParse(Console.ReadLine(), out addedElementPosition) 
                || addedElementPosition < 0 || addedElementPosition > arrayLength - 1)
            {
                Console.WriteLine("Повторите ввод..");
            }

            if (addedElementPosition != arrayLength - 1)
            {
                for (int i = arrayLength - 1; i != addedElementPosition; i--)
                {
                    numbers[i] = numbers[i - 1];
                }
            }

            numbers[addedElementPosition] = addedElement;

            Console.Write("Полученный массив: ");

            foreach (int i in numbers)
            {
                Console.Write($"{i} ");
            }
        }
    }
}
