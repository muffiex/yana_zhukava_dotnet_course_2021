﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 2;
            int number2 = 7;
            int helper = number1;
            number1 = number2;
            number2 = helper;
            Console.WriteLine(number1 + " " + number2);
        }
    }
}
