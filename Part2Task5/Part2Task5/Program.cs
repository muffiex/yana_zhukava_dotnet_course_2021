﻿using System;

namespace Part2Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значения длин сторон треугольника:");

            int firstSide = 0;
            while (!int.TryParse(Console.ReadLine(), out firstSide))
            {
                Console.WriteLine("Повторите ввод..");
            }

            int secondSide = 0;
            while (!int.TryParse(Console.ReadLine(), out secondSide))
            {
                Console.WriteLine("Повторите ввод..");
            }

            int thirdSide = 0;
            while (!int.TryParse(Console.ReadLine(), out thirdSide))
            {
                Console.WriteLine("Повторите ввод..");
            }

            if (firstSide + secondSide > thirdSide && secondSide + thirdSide > firstSide && firstSide + thirdSide > secondSide)
            {
                if (firstSide == secondSide && secondSide == thirdSide)
                {
                    Console.WriteLine("Треугольник существует и является расносторонним.");
                }
                else if (firstSide == secondSide || secondSide == thirdSide || firstSide == thirdSide)
                {
                    Console.WriteLine("Треугольник существует и является равнобедренным.");
                }
                else
                {
                    Console.WriteLine("Треугольник существует.");
                }
            }
            else
            {
                Console.WriteLine("Такой треугольник не существует.");
            }
        }
    }
}
