﻿using System;

namespace P3Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива: ");

            int arrayLength = 0;
            while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength < 0)
            {
                Console.WriteLine("Повторите ввод..");
            }

            int[] numbers = new int[arrayLength];

            Console.WriteLine($"Заполните массив числами: ");

            double value = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                while (!double.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("Повторите ввод..");
                }

                numbers[i] = value;
            }

            Console.Write("Полученный массив: ");

            foreach (int i in numbers)
            {
                Console.Write($"{i} ");
            }

            int positiveElementsCount = 0;

            double sum = 0;

            double average = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                if (numbers[i] > 0)
                {
                    sum += numbers[i];
                    positiveElementsCount++;
                }
            }

            average = sum / positiveElementsCount;

            Console.WriteLine($"\nСреднее арифметическое положительных элементов массива: {average}");
        }
    }
}
