﻿using System;

namespace P3Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива: ");

            int arrayLength = 0;

            while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength < 0)
            {
                Console.WriteLine("Повторите ввод..");
            }

            int[] numbers = new int[arrayLength];

            Random random = new Random();

            Console.Write("Исходный массив: ");

            for (int i = 0; i < arrayLength; i++)
            {
                numbers[i] = random.Next(100);
                Console.Write($"{numbers[i]} ");
            }

            int temporaryValue;

            for (int i = 0; i < arrayLength - 1; i++)
            {
                for (int j = i + 1; j < arrayLength; j++)
                {
                    if (numbers[i] > numbers[j])
                    {
                        temporaryValue = numbers[i];
                        numbers[i] = numbers[j];
                        numbers[j] = temporaryValue;
                    }
                }
            }

            Console.Write("Отсортированный массив: ");

            foreach (int i in numbers)
            {
                Console.Write($"{i} ");
            }
        }
    }
}
