﻿using System;

namespace Part2Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 3 коэффициента квадратного уравнения: ");

            int firstCoefficient = 0;
            while (!int.TryParse(Console.ReadLine(), out firstCoefficient))
            {
                Console.WriteLine("Повторите ввод..");
            }

            int secondCoefficient = 0;
            while (!int.TryParse(Console.ReadLine(), out secondCoefficient))
            {
                Console.WriteLine("Повторите ввод..");
            }

            int freeMember = 0;
            while (!int.TryParse(Console.ReadLine(), out freeMember))
            {
                Console.WriteLine("Повторите ввод..");
            }

            Console.WriteLine("Введите значение переменной:");

            int variable = 0;
            while (!int.TryParse(Console.ReadLine(), out variable))
            {
                Console.WriteLine("Повторите ввод..");
            }

            int result = firstCoefficient * variable * variable + secondCoefficient * variable + freeMember;

            Console.WriteLine($"Значение уравнения: {result}");
        }
    }
}
