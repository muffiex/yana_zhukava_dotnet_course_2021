﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string stop = "";

            while (stop.ToUpper() != "НЕТ")
            {
                Console.Write("1е число: ");
                double firstNumber = GetDoubleNumber();

                Console.Write("2е число: ");
                double secondNumber = GetDoubleNumber();

                Operation operationToDo = GetOperation();

                double result = CountResult(firstNumber, secondNumber, operationToDo);
                Console.WriteLine($"Ответ: {result}.");

                Console.WriteLine("Если желаете очистить консоль нажмите С(с): ");
                string clearConsole = Console.ReadLine();

                if (clearConsole.ToUpper() == "С" || clearConsole.ToUpper() == "C")
                {
                    Console.Clear();
                }

                Console.WriteLine("Желаете продолжить? ");
                stop = Console.ReadLine();
            }
        }
        
        public static double GetDoubleNumber()
        {
            double number = 0;
            while (!Double.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Повторите ввод..");
            }

            return number;
        }

        public static Operation GetOperation()
        {
            const string add = "+";
            const string subtract = "-";
            const string multiply = "*";
            const string divide = "/";

            Console.Write("Введите нужную операцию (+, -, *, /): ");
            string operation = Console.ReadLine();

            while (operation != add && operation != subtract && operation != divide && operation != multiply)
            {
                Console.Write("Неверный ввод..\nВведите нужную операцию (+, -, *, /): ");
                operation = Console.ReadLine();
            }

            Operation operationToDo = 0;
            switch (operation)
            {
                case add:
                    operationToDo = Operation.Add;
                    break;
                case subtract:
                    operationToDo = Operation.Subtract;
                    break;
                case multiply:
                    operationToDo = Operation.Multiply;
                    break;
                case divide:
                    operationToDo = Operation.Divide; 
                    break;
            }
            return operationToDo;
        }

        static double CountResult(double firstNumber, double secondNumber, Operation operationToDo)
        {
            double result = 0;
            switch (operationToDo)
            {
                case Operation.Add:
                    result = firstNumber + secondNumber;
                    break;
                case Operation.Subtract:
                    result = firstNumber - secondNumber;
                    break;
                case Operation.Multiply:
                    result = Math.Round(firstNumber * secondNumber, 2);
                    break;
                case Operation.Divide:
                    while (secondNumber == 0)
                    {
                        Console.Write("На ноль делить нельзя!!\nПовторите ввод 2 числа: ");
                        secondNumber = GetDoubleNumber();
                    }

                    result = Math.Round(firstNumber / secondNumber, 2);
                    break;
            }

            return result;
        }
    }
}
