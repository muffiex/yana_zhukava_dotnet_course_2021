﻿using System;

namespace P3Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите длину массива: ");

            int arrayLength = 0;
            while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength < 0)
            {
                Console.WriteLine("Повторите ввод..");
            }

            int[] numbers = new int[arrayLength];

            Console.WriteLine($"Заполните массив числами: ");

            int value = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                while (!int.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("Повторите ввод..");
                }

                numbers[i] = value;
            }

            Console.Write("Полученный массив: ");

            foreach (int i in numbers)
            { 
                Console.Write($"{i} ");
            }

            int sum = 0;
            for (int i = 0; i < arrayLength; i++)
            {
                numbers[i] = Math.Abs(numbers[i]);

                while (numbers[i] > 0)
                {
                    sum += numbers[i] % 10;
                    numbers[i] = numbers[i] / 10;
                }
            }

            Console.WriteLine($"\nСумма цифр элементов массива: {sum}.");
        }
    }
}
