﻿using System;

namespace Part2Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите год:");

            int year = 0;
            while (!int.TryParse(Console.ReadLine(), out year))
            {
                Console.WriteLine("Мне нужны цифры!!");
            }

            if (year % 4 == 0)
            {
                Console.WriteLine($"{year} год — високосный.");
            }
            else
            {
                Console.WriteLine($"{year} год — не високосный.");
            }
        }
    }
}
