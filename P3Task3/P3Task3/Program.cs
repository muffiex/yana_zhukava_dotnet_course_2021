﻿using System;

namespace P3Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива: ");

            int arrayLength = 0;
            while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength < 0)
            {
                Console.WriteLine("Повторите ввод..");
            }

            int[] numbers = new int[arrayLength];

            Console.WriteLine($"Заполните массив числами: ");

            int value = 0;
            for (int i = 0; i < arrayLength; i++)
            {
                while (!int.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("Повторите ввод..");
                }

                numbers[i] = value;
            }

            Console.Write("Полученный массив: ");

            foreach (int i in numbers)
            {
                Console.Write($"{i} ");
            }

            int maxRepeatCount = 0;

            int repeatCount = 0;

            int maxRepeatedNumber = 0;

            int number = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                for (int j = 0; j < arrayLength; j++)
                {
                    number = numbers[i];
                    if (numbers[i] == numbers[j])
                    {
                        repeatCount++;
                    }
                }

                if (repeatCount > maxRepeatCount)
                {
                    maxRepeatedNumber = number;
                    maxRepeatCount = repeatCount;
                }

                repeatCount = 0;
            }

            Console.WriteLine($"\nЧаще всего ({maxRepeatCount} раз) использовалось число {maxRepeatedNumber}.");
        }
    }
}
