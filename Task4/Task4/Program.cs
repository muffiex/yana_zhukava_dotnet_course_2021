﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите 3 цифры одной строкой(без пробелов): ");
            int numbers = (Convert.ToInt32(Console.ReadLine()));
            int number1 = numbers / 100;
            int number2 = (numbers / 10) % 10;
            int number3 = (numbers % 100) % 10;
            int maxValue = Math.Max(number1, number2);
            maxValue = Math.Max(maxValue, number3);
            Console.WriteLine("Наибольшая цифра: " + maxValue);
        }
    }
}
